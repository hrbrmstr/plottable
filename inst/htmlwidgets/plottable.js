HTMLWidgets.widget({

  name: 'plottable',

  type: 'output',

  factory: function(el, width, height) {

    var plot;
    var x_accessor, y_accessor;

    return {

      renderValue: function(x) {
console.log(x);
        var xScale, yScale, xAxis, yAxis;

        if (x.geom.geom == "bar") {
          plot = new Plottable.Plots.Bar();
        } else if (x.geom.geom == "line") {
          plot = new Plottable.Plots.Line();
        } else if (x.geom.geom == "point") {
          plot = new Plottable.Plots.Scatter();
        }

        if (x.geom.x_scale == "linear") {
          xScale = new Plottable.Scales.Linear();
        } else if (x.geom.x_scale == "category") {
          xScale = new Plottable.Scales.Category();
        } else if (x.geom.x_scale == "log") {
          xScale = new Plottable.Scales.ModifiedLog();
        } else if (x.geom.x_scale == "date") {
          xScale = new Plottable.Scales.Time();
        }

        if (x.geom.y_scale == "linear") {
          yScale = new Plottable.Scales.Linear();
        } else if (x.geom.y_scale == "category") {
          yScale = new Plottable.Scales.Category();
        } else if (x.geom.y_scale == "log") {
          yScale = new Plottable.Scales.ModifiedLog();
        } else if (x.geom.y_scale == "date") {
          yScale = new Plottable.Scales.Time();
        }

        xAxis = new Plottable.Axes.Numeric(xScale, x.geom.x_axis);
        yAxis = new Plottable.Axes.Numeric(yScale, x.geom.y_axis);

        var data = HTMLWidgets.dataframeToD3(x.geom.data);
consoel.log(data);
        var dataset = new Plottable.Dataset(data);

        plot.addDataset(dataset);

        x_accessor = x.geom.x_accessor;
        y_accessor = x.geom.y_accessor;

        plot.x(function(d) { return d[x_accessor]; }, xScale);
        plot.y(function(d) { return d[y_accessor]; }, yScale);

        var svg = d3.select(el)
                    .append("svg")
                    .attr("width", width)
                    .attr("height", height);

        new Plottable.Components.Table([
          [yAxis, plot],
          [null, xAxis]
        ]).renderTo(svg);

      },

      resize: function(width, height) {
        plot.redraw();
      }

    };
  }
});

// plottable(data.frame(x=0:3, y=c(1,2,4,8)))