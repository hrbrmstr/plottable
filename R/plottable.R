#' <Add Title>
#'
#' <Add Description>
#'
#' @import htmlwidgets
#'
#' @export
plt <- function(width = NULL, height = NULL, elementId = NULL) {

  # forward options using x
  x = list(
    geom = NULL
  )

  # create widget
  htmlwidgets::createWidget(
    name = 'plottable',
    x,
    width = width,
    height = height,
    package = 'plottable',
    elementId = elementId
  )
}
