#' @export
plt_bar <- function(pobj,
                    dat=data.frame(x=0:3, y=c(1,2,4,8)),
                    x="x",
                    y="y",
                    x_scale = "linear",
                    y_scale = "linear",
                    orientation=c("horizontal", "vertical"),
                    id=NULL) {

  dat <- data.frame(dat, stringsAsFactors=FALSE)

  if (x_scale == "date") dat[,x] <- as.POSIXct(dat[,x])
  if (y_scale == "date") dat[,y] <- as.POSIXct(dat[,y])

  pobj$x$geom <- list(
    data = dat,
    geom = "bar",
    orientation = orientation,
    x_scale = x_scale,
    y_scale = y_scale,
    x_axis = "bottom",
    y_axis = "left",
    x_accessor = x,
    y_accessor = y,
    id = id
  )

  pobj

}